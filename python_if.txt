@astrosilverio

noted inform 7


original non-framework approach:
- state plus rules about state changes
- state is made of up types (e.g. Rooms, Things, Player), and game state is a big ball of these
- verbs (Commands)

why framework:
- why?  genericized logic rules (very game-specific logic embedded in each command function)
- author is user of framework
- game itself is metadata

what frameworks looks like:
- parser initialized with list of known words (verbs, nouns)
- customizable commands:
-- modularity

commands:
- syntax check
- logic check
- state changes
- output

Command class:
- add helper methods to concrete definitions of commands.
- e.g. add logic to go command for special conditions 
- add commands after every player turn (e.g. do a thing on turn 100)

