@rahaeli

Dreamwidth:
- forked from LJ in 2008
- could have forked or rewrite
- there are benefits to forking:
-- familiar to devs and users, features already exist


Bad stuff:
- comments with 404 urls
- workarounds for ancient browsers
- really old html
- out-of-date special cases (e.g. workarounds for mispellings of no-longer-common email addresses)
- old bad decisions that were right at the time
-- e.g. disabling parts of utf-8 because perl was borked
- reliance on outdated libraries
- ancient javascript
- massive duplication (multiple kinds of search, logging, admin panels, etc)
- bugs that turned into features due to existing for so long
-- e.g. comment cleaner (anti-spam, delink spam)

Should you rewrite:
- cost vs benefit analysis
-- pros:
--- best practices and standards compat
--- easier installation / deployment
--- eliminate project-specific systems (nobody else is using this framework / lib)
--- reduce reliance on institutional knowledge
--- how much will it benefit end users or team?
-- cons:
--- will you lose bugfixes or security fixes?
--- time retraining, block other development
--- tied to something that won't last.  JS frameworks.  avoid shiny new techs.
--- extra cost for forking/adapting existing library

- before pro/con:
-- are you going to finish the rewrite?


Future-proofing:
- comment everything, in the code itself
-- no links
- grep-friendly.  notes to self.  TODO/FIXME/XXX/FUTURE etc.
- task list for future self
-- this is a browser fix, can be removed later
- regularly install/compile from scratch
-- reveals issues you'll run into, find revved packages no longer compatible, etc

